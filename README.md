# Justwifi

JustWifi library for Arduino and ESP8266 using the Arduino Core for ESP8266.

This **was** the official repository for JustWifi library until May 20th, 2018, when it was moved to GitHub.
Please visit the new repository to checkout the latest version of the firmware, download up to date images, read the documentation, report issues or file pull-requests.

> https://github.com/xoseperez/justwifi
